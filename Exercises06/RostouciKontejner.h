#ifndef ROSTOUCIKONTEJNER_H
#define ROSTOUCIKONTEJNER_H
#include <stdexcept> 

template <class TypDat, int PocatecniVelikost = 5, int RoustouciKoeficient = 2>

class RoustouciKontejner
{
public:
	RoustouciKontejner();
	~RoustouciKontejner();
	bool jeMistoVPoli() const;
	void zvetsiPole();
	void pridej(const TypDat& o);
	TypDat& operator[] (int index);
	TypDat operator[] (int index) const;
	unsigned int pocet() const;
private:
	TypDat* _pole;
	unsigned _velikostPole;
	unsigned _pocetPlatnychPrvku;

};

#endif // !ROSTOUCIKONTEJNER_H

template <typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::RoustouciKontejner()
{
	this->_velikostPole = PocatecniVelikost;
	this->_pole = new TypDat[_velikostPole];
	_pocetPlatnychPrvku = 0;
}

template<typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
 RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::~RoustouciKontejner()
{
	 delete[] _pole;
}

 template<typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
  bool RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::jeMistoVPoli() const
 {
	  if (_pocetPlatnychPrvku == _velikostPole)
	  {
		  return false;
	  }

	  else
	  {
		  return true;
	  }
 }

  template<typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
 void RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::zvetsiPole()
  {
	 if (!jeMistoVPoli())
	 {
		 int pomVel = RoustouciKoeficient * _velikostPole;
		 TypDat* pom = new TypDat[pomVel];


		 for (size_t i = 0; i < _velikostPole; i++)
		 {
			 pom[i] = _pole[i];


		 }
		 delete[] _pole;
		 _pole = pom;
		 _velikostPole = pomVel;
	 }
  }

 template<typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
 void RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::pridej(const TypDat& o)
 {
	 if (jeMistoVPoli())
	 {
		 _pole[_pocetPlatnychPrvku++] = o;
	 }
	 else
	 {
		 zvetsiPole();
		 _pole[_pocetPlatnychPrvku++] = o;
	 }


 }

 template<typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
 TypDat& RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::operator[](int index)
 {
	 if (index < _pocetPlatnychPrvku)
	 {
		 return _pole[index];
	 }
	 else
	 {
		 throw std::invalid_argument("Neplatny index");
	 }
 }

 template<typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
TypDat RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::operator[](int index) const
 {
	 if (index < _pocetPlatnychPrvku)
	 {
		 return _pole[index];
	 }
	 else
	 {
		 throw std::invalid_argument("Neplatny index");
	 }
 }

 template<typename TypDat, int PocatecniVelikost, int RoustouciKoeficient>
 unsigned int RoustouciKontejner<TypDat, PocatecniVelikost, RoustouciKoeficient>::pocet() const
 {
	 return this->_pocetPlatnychPrvku;
 }
