#include "RostouciKontejner.h"
#include <iostream>
using namespace std;
int main()
{

    RoustouciKontejner<int> kontejner;
    kontejner.pridej(5);
    kontejner.pridej(4);
    kontejner.pridej(3);
    kontejner.pridej(2);
    kontejner.pridej(1);
    kontejner.pridej(6);

    try
    {
    cout << "Pocet prvku v kontejneru: " << kontejner.pocet()<< endl;
    cout << kontejner[1] << endl;
    cout << kontejner[5] << endl;
    cout << kontejner[6] << endl;
    }
    catch (const std::exception e)
    {
        cout << e.what() << endl;
    }
   
    system("pause");

    return 0;
}
