
#ifndef EXERCISES03_OBJECT_H
#define EXERCISES03_OBJECT_H


class Object {
private:
    int id;
    int x;
    int y;

public:
    Object(int id);
    virtual ~Object() {};

    int getId() const;
    double getX()const;
    double getY()const;

    void setX(double aX);
    void setY(double aY);

};


#endif //EXERCISES03_OBJECT_H
