//
// Created by mredina on 14.10.20.
//

#ifndef EXERCISES03_MOVINGOBJECT_H
#define EXERCISES03_MOVINGOBJECT_H

#include "Object.h"
#include <stdexcept>
#include <tgmath.h>

const double _PI = atan(1)*4;

class MovingObject : public Object {
private:
    double angelRotation;
public:
    MovingObject(int aId , double angleRotation);
    virtual ~MovingObject();

    void setAngelRotation(double aAngelRotation);
    double getAngelRotation() const;
};

#endif //EXERCISES03_MOVINGOBJECT_H
