//
// Created by mredina on 14.10.20.
//

#include "Game.h"
#include "StaticObject.h"

Game::Game() {
    objects = new Object*[MAX_OBJECTS];
}

Game::~Game() {

    for (int i = 0; i < MAX_OBJECTS; ++i) {
        if(objects[i] != nullptr){
                delete objects[i];
        }
    }
    delete[] objects;
}

void Game::addObject(Object* obj) {
    objects[next] = obj;
    next++;
}

int *Game::findIdOfStaticObjects(double xmin, double xmax, double ymin, double ymax) {
    int pocet = 0;
    for (size_t i = 0; i < next; i++){
        Object* obj = objects[i];
        StaticObject* so = dynamic_cast<StaticObject*>(obj);
        if (so == nullptr) {
            continue;
        }
        if (so->getX() > xmin && so->getX() < xmax && so->getY() > ymin && so->getY() < ymax)
            pocet++;
    }

    if (pocet == 0)
        return nullptr;

    int nextToArray = 0;
    int* ids = new int[pocet];
    for (size_t i = 0; i < next; i++) {
        Object* obj = objects[i];
        StaticObject* so = dynamic_cast<StaticObject*>(obj);
        if (so == nullptr) {
            continue;
        }
        if (so->getX() > xmin && so->getX() < xmax && so->getY() > ymin && so->getY() < ymax)
            ids[nextToArray] = obj->getId();
        nextToArray++;
    }

    return ids;

}

MovingObject **Game::findMovingObjectsInArea(double x, double y, double r) const {
    int pocet = 0;
    for (size_t i = 0; i < next; i++) {
        Object * obj = objects[i];
        MovingObject* mo = dynamic_cast<MovingObject*>(obj);
        if (mo == nullptr) {
            continue;
        }
        //(x - center_x)^2 + (y - center_y)^2 < radius^2
        if ( (pow(mo->getX() - x, 2) + pow(mo->getY() - y, 2) < pow(r, 2)) ) {
            pocet++;
        }

    }

    if (pocet == 0)
        return nullptr;

    int nextToArray = 0;
    MovingObject ** movingObjects = new MovingObject*[pocet];
    for (size_t i = 0; i < next; i++) {
        Object* obj = objects[i];
        MovingObject* mo = dynamic_cast<MovingObject*>(obj);
        if (mo == nullptr) {
            continue;
        }
        //(x - center_x)^2 + (y - center_y)^2 < radius^23
        if (pow(mo->getX() - x, 2) + pow(mo->getY() - y, 2) < pow(r, 2)) {
            movingObjects[nextToArray] = mo;
        }
        nextToArray++;
    }

    return movingObjects;

}

MovingObject **Game::findMovingObjectsInArea(double x, double y, double r, double umin, double umax) const {
    int pocet = 0;
    for (size_t i = 0; i < next; i++) {
        Object* obj = objects[i];
        MovingObject* mo = dynamic_cast<MovingObject*>(obj);
        if (mo == nullptr) {
            continue;
        }
        //(x - center_x)^2 + (y - center_y)^2 < radius^2
        if (pow(mo->getX() - x, 2) + pow(mo->getY() - y, 2) < pow(r, 2) && mo->getAngelRotation() >= umin&& mo->getAngelRotation() <= umax){
            pocet++;
        }

    }

    if (pocet == 0)
        return nullptr;

    int nextToArray = 0;
    MovingObject** movingObjects = new MovingObject * [pocet];
    for (size_t i = 0; i < next; i++) {
        Object* obj = objects[i];
        MovingObject* mo = dynamic_cast<MovingObject*>(obj);
        if (mo == nullptr) {
            continue;
        }
        //(x - center_x)^2 + (y - center_y)^2 < radius^23
        if (pow(mo->getX() - x, 2) + pow(mo->getY() - y, 2) < pow(r, 2) && mo->getAngelRotation() >= umin && mo->getAngelRotation() <= umax) {
            movingObjects[nextToArray] = mo;
        }
        nextToArray++;
    }

    return movingObjects;

}
