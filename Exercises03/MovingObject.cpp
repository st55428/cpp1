//
// Created by mredina on 14.10.20.
//

#include "MovingObject.h"

MovingObject::MovingObject(int aId, double aAngelRotation) : Object(aId){
    angelRotation = aAngelRotation;
}

MovingObject::~MovingObject() {}

double MovingObject::getAngelRotation() const {
    return angelRotation;
}

void MovingObject::setAngelRotation(double aAngelRotation) {
    if (aAngelRotation < 0 && aAngelRotation > _PI) //
        throw std::out_of_range("Angle out of range");
    angelRotation = aAngelRotation;
}

