#ifndef EXERCISES03_GAME_H
#define EXERCISES03_GAME_H


#include "Object.h"
#include "MovingObject.h"
//#include "StaticObject.h"

#define MAX_OBJECTS 1000

class Game {
private:
    Object** objects;
    int next = 0;
public:
    Game();
    ~Game();

    void addObject (Object* obj);
    int* findIdOfStaticObjects(double xmin, double xmax, double ymin, double ymax);

    MovingObject** findMovingObjectsInArea(double x, double y, double r) const;
    MovingObject** findMovingObjectsInArea(double x, double y, double r, double umin, double umax) const;

};


#endif //EXERCISES03_GAME_H

