#include <iostream>
#include "math.h"
#include "Game.h"
#include "StaticObject.h"
#include "Monster.h"

int main() {
    Object* objekt = new StaticObject{ 1, Small_flower };
    StaticObject* so = dynamic_cast<StaticObject*>(objekt);
    if (so != nullptr)
        std::cout << "Objekt je StatickyObjekt nebo potomek" << so->getObstacleKind() <<std::endl;
    int counter = 0;
    Game* g = new Game();
    time_t startTime = time(nullptr);
    for (int i = 0; i < 10000; i++)
    {
        Object* o;
        if (rand() % 2 == 0) {
            MovingObject* mo = new MovingObject(counter,1);
            mo->setX(rand() % 100 + 100);
            mo->setY(rand() % 100 + 100);
            o = mo;
        }
        else {
            o = new StaticObject(i,Stone);
        }
        g->addObject(o);
    }
    time_t endTime = time(nullptr);
    char* startBuffer = new char[20];
    char* endBuffer = new char[20];
    std::cout << "Start time: " << startTime << std::endl
              << "End time: " << endTime << std::endl;

    return 0;
}
