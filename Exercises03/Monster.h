//
// Created by mredina on 14.10.20.
//

#ifndef EXERCISES03_MONSTER_H
#define EXERCISES03_MONSTER_H


#include "MovingObject.h"

class Monster : public MovingObject {
private:
    int hp;
    int maxHp;
public:
    Monster(int aId, double aRotationAngle, int aHp, int aMaxHp);
    virtual ~Monster();

    int getHp() const;
    int getMaxHp() const;

    void setHp(int aHp);
    void setMaxHp(int aMaxHp);

};


#endif //EXERCISES03_MONSTER_H
