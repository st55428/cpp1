//
// Created by mredina on 14.10.20.
//

#include "StaticObject.h"

StaticObject::StaticObject(int aId, Obstacle aObstacleList) : Object(aId) {
    aObstacleList = obstacleList;
}

StaticObject::~StaticObject() {

}

Obstacle StaticObject::getObstacleKind() const {
    return obstacleList;
}
