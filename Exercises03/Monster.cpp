//
// Created by mredina on 14.10.20.
//

#include "Monster.h"

Monster::Monster(int aId, double aRotationAngle, int aHp, int aMaxHp) : MovingObject(aId, aRotationAngle)  {
    hp = aHp;
    maxHp = aMaxHp;
}

Monster::~Monster() {

}

int Monster::getHp() const {
    return hp;
}

int Monster::getMaxHp() const {
    return maxHp;
}

void Monster::setHp(int aHp) {
    hp = aHp;
}

void Monster::setMaxHp(int aMaxHp) {
    maxHp = aMaxHp;
}
