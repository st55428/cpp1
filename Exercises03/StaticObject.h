//
// Created by mredina on 14.10.20.
//

#ifndef EXERCISES03_STATICOBJECT_H
#define EXERCISES03_STATICOBJECT_H


#include "Object.h"

enum Obstacle{
    Stone,
    Small_flower,
    Big_flower
};

class StaticObject : public Object {

private:
    Obstacle obstacleList;
public:
    StaticObject(int aId, Obstacle aObstacleKind);
    ~StaticObject();
    Obstacle getObstacleKind() const;
};

#endif //EXERCISES03_STATICOBJECT_H
