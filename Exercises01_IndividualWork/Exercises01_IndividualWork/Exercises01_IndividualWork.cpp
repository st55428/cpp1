﻿#include <iostream>

using namespace std;

typedef struct Trojuhelnik {
	unsigned int a, b, c;
} Trojuhelnik;

bool lzeSestrojit(Trojuhelnik* t) {
	if (!(t->a + t->b > t->c && t->a + t->c > t->b && t->b + t->c > t->a)) {
		return false;
	}
	else {
		return true;
	}
}

int main()
{
	unsigned int pocet = 0;

	std::cout << "Kolik trojuhelniku:" << std::endl;
	std::cin >> pocet;

	if (pocet < 0) {
		std::cout << "To jako fakt? Zaporny cisla neberu..." << std::endl;
		return 1;
	}

	Trojuhelnik* trojuhelniky = new Trojuhelnik[pocet];

	for (; pocet > 0; --pocet) {
		Trojuhelnik* t = &(trojuhelniky[pocet - 1]);

		std::cout << "------------------------" << std::endl;
		std::cout << "Zadejte stranu a: " << std::endl;
		std::cin >> t->a;
		std::cout << "Zadejte stranu b: " << std::endl;
		std::cin >> t->b;
		std::cout << "Zadejte stranu c: " << std::endl;
		std::cin >> t->c;

		if (!lzeSestrojit(t)) {
			std::cout << "Trojuhelnik nelze sestrojit!" << std::endl;
			continue;
		}

		unsigned int o = t->a + t->b + t->c;
		std::cout << "Obvod trojuhelniku je:" << o << std::endl;
	}


	delete[] trojuhelniky;

	return 0;
}



