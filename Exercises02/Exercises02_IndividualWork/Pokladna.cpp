#include "Pokladna.h"

#define ID_COUNTER 1000

int Pokladna::citacId = ID_COUNTER;

Pokladna::Pokladna() {
	poleUctenek = new Uctenka[10];

}

Pokladna::~Pokladna()
{
	delete[] poleUctenek;
}

Uctenka& Pokladna::vystavUctenku(double aCastka, double aDph)
{
	poleUctenek[pocetVydanychUctenek].setCisloUctenky(citacId++);
	poleUctenek[pocetVydanychUctenek].setCastka(aCastka);
	poleUctenek[pocetVydanychUctenek].setDph(aDph);
	pocetVydanychUctenek++;
	return poleUctenek[pocetVydanychUctenek];
}

Uctenka& Pokladna::dejUctenku(int aCislo)
{
	for (int i = 0; i < pocetVydanychUctenek; i++)
	{
		if (poleUctenek[i].getCisloUctenky() == aCislo)
		{
			return poleUctenek[i];
		}
	}
	return poleUctenek[0];
}

double Pokladna::dejCastku() const
{
	int vysledek = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++)
	{
		vysledek += poleUctenek[i].getCastka();
	}
	return vysledek;
}

double Pokladna::dejCastkuVcDph() const
{
	int vysledek = 0;
	int castkaVcDph = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++)
	{
		castkaVcDph = poleUctenek[i].getCastka() + (poleUctenek[i].getCastka() * (poleUctenek[i].getDph() / 100));
		vysledek += castkaVcDph;
	}
	return vysledek;
}
