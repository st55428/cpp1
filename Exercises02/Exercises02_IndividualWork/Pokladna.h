#ifndef ICPP1_POKLADNA_H
#define ICPP1_POKLADNA_H

#include "Uctenka.h"

class Pokladna {

private:
	Uctenka* poleUctenek;
	static int citacId;
	int pocetVydanychUctenek = 0;

public:
	Pokladna();
	~Pokladna();

	Uctenka& vystavUctenku(double aCastka, double aDph);
	Uctenka& dejUctenku(int aCislo);

	double dejCastku() const;
	double dejCastkuVcDph() const;

};

#endif // !ICPP_POKLADNA_H





