#ifndef ICPP_UCTENKA_H
#define ICPP_UCTENKA_H

class Uctenka {
private:

	double castka;
	double dph;
	int cisloUctenky;

public:
	Uctenka();
	~Uctenka();

	int getCisloUctenky() const;
	void setCisloUctenky(int aCisloUctenky);

	double getCastka()const;
	void setCastka(double aCastka);

	double getDph()const;
	void setDph(double aDph);

};
#endif // !ICPP_UCTENKA_H