﻿#include <iostream>
#include "Pokladna.h"

using namespace std;

int main()
{
    Pokladna pokladna{};
    
    pokladna.vystavUctenku(100.49,20.0);
    pokladna.vystavUctenku(200.9, 50.1);
    pokladna.vystavUctenku(150.0, 21.25);
    
    Uctenka& u = pokladna.dejUctenku(1000);

    int castka = pokladna.dejCastku();

    int castkaVcDph = pokladna.dejCastkuVcDph();

    cout << u.getCastka() << "  " << u.getCisloUctenky() << endl;

    cout << "castka: " << castka << endl;

    cout << "castka vcetne dph: " << castkaVcDph << endl;

    system("pause");

    return 0;
}
