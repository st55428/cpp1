#include <time.h>
#include <iostream>
#include "Osoba.h"
#include "TelefoniSeznam.h"

int main() {
	Model::TelefoniSeznam* ts = new Model::TelefoniSeznam();
	ts->pridejOsobu(Entity::Osoba("jmeno", "123456789", 1));
	ts->pridejOsobu(Entity::Osoba("jmeno2", "987654321", 120));
	ts->pridejOsobu(Entity::Osoba("jmeno3", "111222333", 9));

	try {
		ts->najdiTelefon(120);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	}

	system("pause");
	return 0;

	
}
