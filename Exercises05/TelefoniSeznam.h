#ifndef TELEFONI_SEZNAM_H
#define TELEFONI_SEZNAM_H

#include <string>
#include "Osoba.h"


namespace Model {
	class TelefoniSeznam
	{
	public:
		TelefoniSeznam();
		~TelefoniSeznam();
		void pridejOsobu(Entity::Osoba aOsoba);
		std::string najdiTelefon(std::string aJmeno) const;
		std::string najdiTelefon(int aId)const;

		class PrvekSeznam 
		{
		public:
			PrvekSeznam();
			~PrvekSeznam();
			Entity::Osoba data;
			PrvekSeznam* dalsi;
		};

	private:
		PrvekSeznam* zacatek;
	};
}

#endif // _TELEFONI_SEZNAM_H

