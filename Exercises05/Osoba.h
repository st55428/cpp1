#ifndef OSOBA_H
#define OSOBA_H

#include <string>

namespace Entity
{
	class Osoba
	{
	public:
		Osoba() {};
		Osoba(std::string aJmeno, std::string aTelefon, int aId);
		~Osoba();
		std::string getJmeno()const;
		std::string getTelefon()const;
		int getId()const;
	private:
		int id;
		std::string jmeno;
		std::string telefon;
	};
}

#endif // !OSOBA_H



