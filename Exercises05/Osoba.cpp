#include "Osoba.h"


Entity::Osoba::Osoba(std::string aJmeno, std::string aTelefon, int aId)
{
	jmeno = aJmeno;
	telefon = aTelefon;
	id = aId;
}

Entity::Osoba::~Osoba()
{
}

std::string Entity::Osoba::getJmeno() const
{
	return jmeno;
}

std::string Entity::Osoba::getTelefon() const
{
	return telefon;
}

int Entity::Osoba::getId() const
{
	return id;
}
