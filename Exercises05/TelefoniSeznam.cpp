#include "TelefoniSeznam.h"
#include "Osoba.h"
#include <stdexcept>

Model::TelefoniSeznam::TelefoniSeznam()
{
	zacatek= nullptr;
}

Model::TelefoniSeznam::~TelefoniSeznam()
{
	PrvekSeznam* prvek = zacatek;
	while (prvek != nullptr) {
		PrvekSeznam* tmp = prvek->dalsi;
		delete prvek;
		prvek = tmp;
	}
}

void Model::TelefoniSeznam::pridejOsobu(Entity::Osoba aOsoba)
{
	PrvekSeznam* prvek = new PrvekSeznam();
	prvek->data = aOsoba;
	prvek->dalsi = zacatek;
	zacatek = prvek;

}

std::string Model::TelefoniSeznam::najdiTelefon(std::string aJmeno) const
{
	if (aJmeno.size() == 0) {
		throw std::invalid_argument("Zadny parametr");
	}
	PrvekSeznam* prvek = zacatek;
	while (prvek != nullptr) {
		if (prvek->data.getJmeno() == aJmeno) {
			return prvek->data.getTelefon();
		}
		prvek = prvek->dalsi;
	}
	throw std::invalid_argument("Neexistuje");
}

std::string Model::TelefoniSeznam::najdiTelefon(int aId) const
{
	if (aId < 0) {
		throw std::invalid_argument("Zadny parametr");
	}
	PrvekSeznam* prvekSeznam = zacatek;
	while (prvekSeznam != nullptr) {
		if (prvekSeznam->data.getId() == aId) {
			return prvekSeznam->data.getTelefon();
		}
		prvekSeznam = prvekSeznam->dalsi;
	}
	throw std::invalid_argument("Neexistuje");

}

Model::TelefoniSeznam::PrvekSeznam::PrvekSeznam()
{
}

Model::TelefoniSeznam::PrvekSeznam::~PrvekSeznam()
{
}
