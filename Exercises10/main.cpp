#include <iostream>
#include "string"
#include "Potrubi.h"

# define MAXSOUBORU 7

using namespace std;

int main() {
    string line;
    int velikost;
    Potrubi p = Potrubi(0);
    for (int i = 0; i < MAXSOUBORU; i++)
    {
        std:stringstream souborSpojeni;
        souborSpojeni << "./soubory/soubor" << i << ".txt";

        string soubor = souborSpojeni.str();
        std::ifstream file(soubor, std::ifstream::in);

        if (file.is_open())
        {
            std::getline(file, line);
            velikost = stoi(line);
            p = Potrubi(velikost);
            file >> p;
            cout << "Otevren soubor "<< i << " >> potrubi je [0/NOK;1/OK]:  ";
            cout << p.JePotrubiOk() << endl;

        }
        else
        {
            cout << "Chyba pri otevirani souboru" << endl;
        }

    }
    return 0;

}
