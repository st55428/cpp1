#include "Osoba.h"
#include <fstream>
using namespace std;

std::ostream& operator<<(std::ostream& vystup, const Adresa& a)
{
	vystup << a._mesto << " " << a._ulice << " " << a._psc << " " << std::endl;
	return vystup;
}

std::istream& operator>>(std::istream& vstup, Adresa& a)
{
	vstup >> a._mesto;
	vstup >> a._ulice;
	vstup >> a._psc;
	return vstup;
}

void ulozBin(std::ofstream& output, const Adresa& a)
{
	int delka = a._mesto.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(a._mesto.c_str(), delka);

	delka = a._ulice.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(a._ulice.c_str(), delka);

	output.write((const char*)&a._psc, sizeof(int));

}

void nactiBin(std::ifstream& input, Adresa& a)
{
	char* pom;
	int delka = 0;

	input.read((char*) & (delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	a._mesto.append(pom, delka);

	input.read((char*) & (delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	a._ulice.append(pom, delka);

	input.read((char*) & (a._psc), sizeof(int));

}

std::ostream& operator<<(std::ostream& vystup, const Datum& dat)
{
	vystup << dat._datum << " " << dat._mesic << " " << dat._rok << " " << std::endl;
	return vystup;
}

std::istream& operator>>(std::istream& vstup, Datum& dat)
{
	vstup >> dat._datum;
	vstup >> dat._mesic;
	vstup >> dat._rok;
	return vstup;
}

void ulozBin(std::ofstream& output, const Datum& d)
{
	output.write((const char*)&d._datum, sizeof(int));
	output.write((const char*)&d._mesic, sizeof(int));
	output.write((const char*)&d._rok, sizeof(int));
}

void nactiBin(std::ifstream& input, Datum& d)
{
	input.read((char*)&d._datum, sizeof(int));
	input.read((char*)&d._mesic, sizeof(int));
	input.read((char*)&d._rok, sizeof(int));

}

std::ostream& operator<<(std::ostream& vystup, const Osoba& o)
{
	vystup << o.jmeno << " " << o.prijmeni << " " << o.datum << " " << o.adresa << " " << std::endl;
	return vystup;
}

std::istream& operator>>(std::istream& vstup, Osoba& o)
{
	vstup >> o.jmeno;
	vstup >> o.prijmeni;
	vstup >> o.datum;
	vstup >> o.adresa;
	return vstup;
}

void ulozBin(std::ofstream& output, const Osoba& os)
{
	int delka = os.jmeno.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(os.jmeno.c_str(), delka);

	delka = os.prijmeni.size() + 1;
	output.write((const char*)&delka, sizeof(int));
	output.write(reinterpret_cast<char*>(&delka), sizeof(int));
	output.write(os.prijmeni.c_str(), delka);

	output << os.adresa;
	output << os.datum;

}

void nactiBin(std::ifstream& input, Osoba& os)
{
	int delka = 0;
	char* pom;
	input.read((char*) & (delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	os.jmeno.append(pom, delka);

	input.read((char*) & (delka), sizeof(int));
	input.read(reinterpret_cast<char*>(&delka), sizeof(int));
	pom = new char[delka];
	input.read(pom, delka);
	os.prijmeni.append(pom, delka);

	input >> os.adresa;
	input >> os.datum;

}
