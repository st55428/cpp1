#ifndef OSOBA_H
#define OSOBA_H

#include <string>
#include <iostream>

struct Datum
{
	int _datum;
	int _mesic;
	int _rok;

};
std::ostream& operator<<(std::ostream& output, const Datum& d);
std::istream& operator>>(std::istream& input, Datum& d);



void ulozBin(std::ofstream& output, const Datum& d);
void nactiBin(std::ifstream& input, Datum& d);


struct Adresa
{
	std::string _ulice;
	std::string _mesto;
	int _psc;

};
std::ostream& operator<<(std::ostream& output, const Adresa& a);
std::istream& operator>>(std::istream& input, Adresa& a);

void ulozBin(std::ofstream& output, const Adresa& a);
void nactiBin(std::ifstream& input, Adresa& a);


struct Osoba
{
	std::string jmeno;
	std::string prijmeni;
	Adresa adresa;
	Datum datum;
};

std::ostream& operator<<(std::ostream& output, const Osoba& os);
std::istream& operator>>(std::istream& input, Osoba& os);

void ulozBin(std::ofstream& output, const Osoba& os);
void nactiBin(std::ifstream& input, Osoba& os);

#endif // !OSOBA_H