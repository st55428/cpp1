#include <iostream>
#include <fstream>
#include "Osoba.h";

#define MAXVELIKOST 5
using namespace std;


void uloz()
{
	Adresa adr = { "Mesto", "Ulice",12345 };
	Datum dat = { 1,1,1990 };
	Osoba osoby[MAXVELIKOST];
	osoby[0].jmeno = "Michal";
	osoby[1].jmeno = "Antonin";
	osoby[2].jmeno = "Radek";
	osoby[3].jmeno = "Tereza";
	osoby[4].jmeno = "Adela";

	osoby[0].prijmeni = "Bozi";
	osoby[1].prijmeni = "Mlady";
	osoby[2].prijmeni = "Stary";
	osoby[3].prijmeni = "Prekrasna";
	osoby[4].prijmeni = "Pekna";


	osoby[0].adresa = adr;
	osoby[1].adresa = adr;
	osoby[2].adresa = adr;
	osoby[3].adresa = adr;
	osoby[4].adresa = adr;



	osoby[0].datum = dat;
	osoby[1].datum = dat;
	osoby[2].datum = dat;
	osoby[3].datum = dat;
	osoby[4].datum = dat;

	std::ofstream soubor;

	soubor.open("soubor.txt");

	if (soubor.is_open())
	{
		soubor << MAXVELIKOST << std::endl;
		soubor << osoby[0];
		soubor << osoby[1];
		soubor << osoby[2];
		soubor << osoby[3];
		soubor << osoby[4];
		soubor.close();
		cout << "Ulozeni probehlo uspesne" << endl;
	}
	else
	{
		cout << "Ulozeni neprobehlo" << endl;

	}
	
}
	


void nacti()
{
	int velikost = MAXVELIKOST;
	std::ifstream soubor{};
	soubor.open("soubor.txt");
	soubor >> velikost;

	Osoba* osoby = new Osoba[MAXVELIKOST];

	for (int i = 0; i < MAXVELIKOST; i++)
	{
		soubor >> osoby[i];
		std::cout << "Osoba: " << osoby[i] << std::endl;
	}

	delete[] osoby;
}

void ulozBin(std::ofstream file, Osoba o)
{
	Adresa adr = { "Mesto", "Ulice",12345 };
	Datum dat = { 1,1,1990 };

	Osoba osoby[MAXVELIKOST];
	
	osoby[0].jmeno = "Michal";
	osoby[1].jmeno = "Antonin";
	osoby[2].jmeno = "Radek";
	osoby[3].jmeno = "Tereza";
	osoby[4].jmeno = "Adela";

	osoby[0].prijmeni = "Bozi";
	osoby[1].prijmeni = "Mlady";
	osoby[2].prijmeni = "Stary";
	osoby[3].prijmeni = "Prekrasna";
	osoby[4].prijmeni = "Pekna";


	osoby[0].adresa = adr;
	osoby[1].adresa = adr;
	osoby[2].adresa = adr;
	osoby[3].adresa = adr;
	osoby[4].adresa = adr;



	osoby[0].datum = dat;
	osoby[1].datum = dat;
	osoby[2].datum = dat;
	osoby[3].datum = dat;
	osoby[4].datum = dat;


	std::ofstream soubor;
	soubor.open("souborBin.bin");
	int delka = MAXVELIKOST;
	soubor.write((const char*)&delka, sizeof(int));
	ulozBin(soubor, osoby[0]);
	ulozBin(soubor, osoby[1]);
	ulozBin(soubor, osoby[2]);
	ulozBin(soubor, osoby[3]);
	ulozBin(soubor, osoby[4]);

	soubor.close();
}

void nactiBin(std::ifstream file, Osoba o)
{
	int velikost = 0;
	std::ifstream vstup{};

	vstup.open("souborBin.bin");

	vstup.read((char*)&velikost, sizeof(int));
	Osoba* osoba = new Osoba[MAXVELIKOST];

	for (int i = 0; i < MAXVELIKOST; i++) {	
		nactiBin(vstup, osoba[i]);
		std::cout << osoba[i] << std::endl;
	}
	delete[] osoba;
	vstup.close();
}

int main()
{
	uloz();
	nacti();
	system("pause");
	return 0;

}