//
// Created by mredina on 22.10.20.
//

#ifndef EXERCISES04_IOBJECT_H
#define EXERCISES04_IOBJECT_H

#include "string"

class IObject {
public:
    IObject(){}
    virtual ~IObject(){}
    virtual std::string toString() const = 0;
};

#endif //EXERCISES04_IOBJECT_H
