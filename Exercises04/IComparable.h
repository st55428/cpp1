//
// Created by mredina on 22.10.20.
//

#ifndef EXERCISES04_ICOMPARABLE_H
#define EXERCISES04_ICOMPARABLE_H

#include "IObject.h"

class IComparable : public IObject{
public:
    IComparable(){};
    virtual ~IComparable(){};
    virtual int compareTo(IComparable* obj) const =0;
};

#endif //EXERCISES04_ICOMPARABLE_H
