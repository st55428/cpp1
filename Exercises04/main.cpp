#include <iostream>
#include "IComparable.h"
#include "Cas.h"

#define VELIKOST_POLE 10


void seraditPole(IComparable** pole, int velikostPole){

    bool prohozeni;
    for (int i = 0; i < velikostPole-1; i++)
    {
        prohozeni = false;
        for (int j = 0; j < velikostPole-i-1; j++)
        {
            if (pole[j]->compareTo(pole[j+1]) == 1)
            {
                IComparable * temp = pole[j];
                pole[j] = pole[j+1];
                pole[j+1] = temp;
                prohozeni = true;
            }
        }

        if (prohozeni == false)
            break;
    }

}

int main() {
    std::cout << "Hello, World!" << std::endl;

    Cas ** casy = new Cas*[VELIKOST_POLE];
    for (int i = 0; i < VELIKOST_POLE; ++i) {
        casy[i] = new Cas(rand() % 25,rand() % 61,rand() % 61);
        std::cout << "Cas Vytvořeni: " << casy[i]->toString() << std::endl;
    }

    seraditPole((IComparable**)casy, VELIKOST_POLE);

    std::cout << std::endl;

    for (int i = 0; i < VELIKOST_POLE; ++i) {
        std::cout << "Cas Seřazeny: " << casy[i]->toString() << std::endl;
    }

    delete[] casy;
    return 0;
}
