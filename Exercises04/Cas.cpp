//
// Created by mredina on 22.10.20.
//

#include "Cas.h"

Cas::Cas(int aHodiny, int aMinuty, int aSekundy) {
    _hodiny = aHodiny;
    _minuty = aMinuty;
    _sekundy = aSekundy;

}

Cas::~Cas() {

}

int Cas::compareTo(IComparable *obj) const {
    Cas* cas2 = dynamic_cast<Cas*>(obj);
    if (cas2 == nullptr)
        return -2;

    if (_hodiny > cas2->_hodiny)
        return 1;
    if (_hodiny < cas2->_hodiny)
        return -1;
    if (_minuty > cas2->_minuty)
        return 1;
    if (_minuty < cas2->_minuty)
        return -1;
    if (_sekundy > cas2->_sekundy)
        return 1;
    if (_sekundy < cas2->_sekundy)
        return -1;

    return 0;
}

std::string Cas::toString() const {
    return std::to_string(_hodiny) + ":" + std::to_string(_minuty) + ":" + std::to_string(_sekundy);
}
