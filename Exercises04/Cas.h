//
// Created by mredina on 22.10.20.
//

#ifndef EXERCISES04_CAS_H
#define EXERCISES04_CAS_H

#include "string"
#include "IComparable.h"

class Cas : public IComparable {
private:
    int _hodiny;
    int _minuty;
    int _sekundy;

public:
    Cas(int aHodiny, int aMinuty, int aSekundy);
    virtual ~Cas();


    int compareTo(IComparable *obj) const override;
    std::string toString() const override;

};


#endif //EXERCISES04_CAS_H
